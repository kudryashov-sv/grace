all: build

build: bin/gnotifier

bin/gnotifier:
	go build -o ./bin/gnotifier ./cmd/gnotifier

.PHONY: clean test lint

clean:
	rm -rf ./bin/*

test:
	go test -cover -count=1 ./...
